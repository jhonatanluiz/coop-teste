package br.com.jj.coop.cooptest.model.enuns;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
public enum Perfil {

    ADMINISTRADOR,
    COMUM;

}