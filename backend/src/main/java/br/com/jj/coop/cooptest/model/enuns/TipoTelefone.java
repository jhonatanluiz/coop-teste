package br.com.jj.coop.cooptest.model.enuns;

public enum TipoTelefone {

    COMERCIAL, CELULAR, RESIDENCIAL;
}
