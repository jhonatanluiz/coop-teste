import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Cliente from './cliente';
import ClienteDetail from './cliente-detail';
import ClienteUpdate from './cliente-update';
import ClienteDeleteDialog from './cliente-delete-dialog';
import PrivateRoute from "app/shared/auth/private-route";
import {AUTHORITIES} from "app/config/constants";

const Routes = ({ match }) => (
  <>
    <Switch>
      <PrivateRoute exact path={`${match.url}/new`} component={ClienteUpdate} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
      <PrivateRoute exact path={`${match.url}/:id/edit`} component={ClienteUpdate} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ClienteDetail}/>
      <ErrorBoundaryRoute path={match.url} component={Cliente} />
    </Switch>
    <PrivateRoute exact path={`${match.url}/:id/delete`} component={ClienteDeleteDialog} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
  </>
);

export default Routes;
