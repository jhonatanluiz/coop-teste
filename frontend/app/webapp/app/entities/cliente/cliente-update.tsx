import React, {useState, useEffect} from 'react';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col} from 'reactstrap';
import {Translate, translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useForm, Controller, useFieldArray} from "react-hook-form";
import {getEntity, updateEntity, createEntity} from './cliente.reducer';

import {useAppDispatch, useAppSelector} from 'app/config/store';

import {yupResolver} from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import {getCEP} from "app/entities/endereco/endereco.reducer";
import InputMask from 'react-input-mask';


export const ClienteUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const validationSchema = Yup.object().shape({
    noCliente: Yup.string()
      .required(translate('entity.validation.required'))
      .min(3, translate('entity.validation.minlength', {min: 3}))
      .max(100, translate('entity.validation.maxlength', {max: 100})),
    nuCpf: Yup.string().required(translate('entity.validation.required')),
    emails: Yup.array().of(
      Yup.object().shape({
        dsEmail: Yup.string()
          .email(translate('entity.validation.email'))
          .required(translate('entity.validation.required'))
      })
    ),
    telefones: Yup.array().of(
      Yup.object().shape({
        nuTelefone: Yup.string().required(translate('entity.validation.required'))
      })
    ),
    endereco: Yup.object().shape({
      nuCep: Yup.string()
        .required(translate('entity.validation.required')),
      dsLogradouro: Yup.string()
        .required(translate('entity.validation.required')),
      coUf: Yup.string()
        .required(translate('entity.validation.required'))
        .max(2, translate('entity.validation.maxlength', {max: 2})),
      noBairro: Yup.string()
        .required(translate('entity.validation.required')),
      noCidade: Yup.string()
        .required(translate('entity.validation.required'))

    })
  });

  const formOptions = {
    resolver: yupResolver(validationSchema)
  };

  const {register, handleSubmit, formState: {errors}, control, setValue, reset} = useForm(formOptions);

  const { fields: emails, append: addEmail, remove: removeEmail} = useFieldArray({
    control,
    name: "emails",
    keyName: "Key"
  });

  const { fields: telefones, append: addTelefone, remove: removeTelefone} = useFieldArray({
    control,
    name: "telefones",
    keyName: "Key"
  });

  const onSubmit = data => {
    // eslint-disable-next-line no-console
    console.log(data);
    saveEntity(data);
  };

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const clienteEntity = useAppSelector(state => state.cliente.entity);
  const loading = useAppSelector(state => state.cliente.loading);
  const updating = useAppSelector(state => state.cliente.updating);
  const updateSuccess = useAppSelector(state => state.cliente.updateSuccess);
  const enderecoEntity = useAppSelector(state => state.endereco.entity);

  const handleClose = () => {
    props.history.push('/cliente' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {

      reset({
        noCliente: '',
        nuCpf: '',
        emails: [],
        telefones:[],
        endereco: {}
      });

      addEmail({dsEmail: '', id:null});
      addTelefone({nuTelefone: '', coTipoTelefone:'CELULAR', id: null});
    } else {
      dispatch(getEntity(props.match.params.id));
    }

  }, [register, reset]);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }

    if(Object.keys(enderecoEntity).length !== 0) {
       setValue("endereco",{
         "nuCep": enderecoEntity.nuCep,
        "dsLogradouro": enderecoEntity.dsLogradouro,
        "noBairro": enderecoEntity.noBairro,
        "noCidade": enderecoEntity.noCidade,
        "coUf": enderecoEntity.coUf
      }, {
        shouldValidate: true,
        shouldDirty: true
      })
    }

  }, [updateSuccess, enderecoEntity]);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }

    if(Object.keys(clienteEntity).length !== 0 && !isNew) {

      reset({
        noCliente: '',
        nuCpf: '',
        emails: [],
        telefones:[],
        endereco: {}
      });

      setValue("noCliente", clienteEntity.noCliente);
      setValue("nuCpf", clienteEntity.nuCpf);

      clienteEntity.emails.forEach((item) => {
        addEmail(item)
      })

      clienteEntity.telefones.forEach((item) => {
        addTelefone(item)
      })

      setValue("endereco",{
        "dsLogradouro": clienteEntity.endereco.dsLogradouro,
        "dsComplemento": clienteEntity.endereco.dsComplemento,
        "noBairro": clienteEntity.endereco.noBairro,
        "noCidade": clienteEntity.endereco.noCidade,
        "coUf": clienteEntity.endereco.coUf
      })

      setValue("endereco.nuCep", clienteEntity.endereco.nuCep)
    }

  }, [updateSuccess, clienteEntity]);

  const saveEntity = values => {
    const entity = {
      ...clienteEntity,
      ...values
    };

    entity.nuCpf = entity.nuCpf.replace(/\D/g, '')

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };


  const handleChangeCEP = (e) => {
    const cep = e.target.value.replace(/\D/g, '');

    if (cep.length === 8) {
      dispatch(getCEP(cep));
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="12">
          <h2 id="coopeforteApp.cliente.home.createOrEditLabel" data-cy="ClienteCreateUpdateHeading">
            <Translate contentKey="coopeforteApp.cliente.home.createOrEditLabel">Create or edit a Cliente</Translate>
          </h2>
        </Col>
      </Row>

        <form onSubmit={handleSubmit(onSubmit)}>
          <Row>
            <Col md={6}>
              <label htmlFor="noCliente" className="form-label">{translate('coopeforteApp.cliente.noCliente')}</label>
              <input id="noCliente"
                     className={errors.noCliente ? "is-invalid form-control" : "form-control"} {...register('noCliente')}
                     aria-invalid={errors.noCliente ? "true" : "false"}/>
              <div className="invalid-feedback">{errors.noCliente?.message}</div>
            </Col>
            <Col md={2}>
              <label htmlFor="nuCpf" className="form-label">{translate('coopeforteApp.cliente.nuCpf')}</label>

              <Controller
                name="nuCpf"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <InputMask mask="999.999.999-99" value={value} onChange={onChange}>
                    {(inputProps) => (
                      <input
                        {...inputProps}
                        type="text"
                        disableUnderline
                        className={errors.nuCpf ? "is-invalid form-control" : "form-control"}
                        aria-invalid={errors.nuCpf ? true : false}
                      />
                    )}
                  </InputMask>
                )}
              />

              <div className="invalid-feedback">{errors.nuCpf?.message}</div>

            </Col>
          </Row>
          {emails.map((item, index) => {
            const fieldName = `emails.${index}.dsEmail`;
            const idName = `emails_${index}_dsEmail`;

            return <Row key={idName}>

              <Col md={6}>
                <input hidden {...register(`emails.${index}.id`)} />

                <label className="form-label">{translate('coopeforteApp.cliente.dsEmail')}</label>
                <input className={errors.emails?.[index]?.dsEmail ? "is-invalid form-control" : "form-control"} {...register(fieldName)}
                  aria-invalid={errors.emails?.[index]?.dsEmail ? true : false}/>
                <div className="invalid-feedback">{errors.emails?.[index]?.dsEmail?.message}</div>
              </Col>
              <Col md={6} style={{paddingTop: "30px"}}>
                <Button id="remove-email" replace color="danger" onClick={() => {removeEmail(index)}}>
                  <FontAwesomeIcon icon="trash"/>
                </Button>
              </Col>
            </Row>
          })}

          &nbsp;
          <Row>
            <Col md={2}>
              <Button id="adicionar-email" replace color="secondary" onClick={() => {addEmail({dsEmail: null, id: null})}}>
                <FontAwesomeIcon icon="plus"/>
                &nbsp;
                <span className="d-none d-md-inline">
                      Adicionar E-mail
                    </span>
              </Button>
            </Col>
          </Row>
          {telefones.map((item, index) => {
            const fieldName = `telefones.${index}.nuTelefone`;
            const idName = `telefones_${index}_nuTelefone`;
            const fieldTipoTelefone = `telefones.${index}.coTipoTelefone`;

            return <Row key={idName}>
              <Col md={2}>
                <input hidden {...register(`telefones.${index}.id`)} />
                <label className="form-label">{translate('coopeforteApp.cliente.nuTelefone')}</label>
                <Controller
                  name={fieldName}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <InputMask mask="(99) 99999-9999" value={value} onChange={onChange}>
                      {(inputProps) => (
                        <input
                          {...inputProps}
                          type="text"
                          disableUnderline
                          className={errors.telefones?.[index]?.nuTelefone ? "is-invalid form-control" : "form-control"}
                          aria-invalid={errors.telefones?.[index]?.nuTelefone ? true : false}
                        />
                      )}
                    </InputMask>
                  )}
                />

                <div className="invalid-feedback">{errors.telefones?.[index]?.nuTelefone?.message}</div>
              </Col>
              <Col md={2}>
                <label className="form-label">Tipo Telefone</label>
                <select name={fieldTipoTelefone} {...register(fieldTipoTelefone)} className={"form-control"}>
                  {['CELULAR', 'COMERCIAL', 'RESIDENCIAL'].map(i =>
                    <option key={i} value={i}>{i}</option>
                  )}
                </select>
              </Col>
              <Col md={1} style={{paddingTop: "30px"}}>
                <Button id="remover-telefone" replace color="danger" onClick={() => {removeTelefone(index)}}>
                  <FontAwesomeIcon icon="trash"/>
                </Button>
              </Col>
            </Row>
          })
          }

          &nbsp;
          <Row>
            <Col md={2}>
              <Button id="adicionar-telefone" replace color="secondary" onClick={() => (addTelefone({dsEmail: '', coTipoTelefone: 'CELULAR', id: null}))}>
                <FontAwesomeIcon icon="plus"/>
                &nbsp;
                <span className="d-none d-md-inline">
                      Adicionar Telefone
                    </span>
              </Button>
            </Col>
          </Row>
          &nbsp;
          <Row>
            <Col md={2}>
              <label className="form-label">{translate('coopeforteApp.cliente.nuCep')}</label>
              <Controller
                name="endereco.nuCep"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <InputMask mask="99.999-999" value={value} onChange={(e) => {handleChangeCEP(e)}}>
                    {(inputProps) => (
                      <input
                        {...inputProps}
                        type="text"
                        disableUnderline
                        className={errors?.endereco?.nuCep ? "is-invalid form-control" : "form-control"}
                        aria-invalid={errors?.endereco?.nuCep ? true : false}
                      />
                    )}
                  </InputMask>
                )}
              />
              <div className="invalid-feedback">{errors?.endereco?.nuCep?.message}</div>
            </Col>
            <Col md={4}>
              <label htmlFor="dsLogradouro"
                     className="form-label">{translate('coopeforteApp.cliente.dsLogradouro')}</label>
              <input id="dsLogradouro"
                     className={errors?.endereco?.dsLogradouro ? "is-invalid form-control" : "form-control"} {...register('endereco.dsLogradouro')}
                     aria-invalid={errors?.endereco?.dsLogradouro ? "true" : "false"}/>
              <div className="invalid-feedback">{errors?.endereco?.dsLogradouro?.message}</div>
            </Col>
            <Col md={2}>
              <label htmlFor="noBairro" className="form-label">{translate('coopeforteApp.cliente.noBairro')}</label>
              <input id="noBairro"
                     className={errors?.endereco?.noBairro ? "is-invalid form-control" : "form-control"} {...register('endereco.noBairro')}
                     aria-invalid={errors?.endereco?.noBairro ? "true" : "false"}/>
              <div className="invalid-feedback">{errors?.endereco?.noBairro?.message}</div>
            </Col>
          </Row>
          <Row>

            <Col md={2}>
              <label htmlFor="noCidade" className="form-label">{translate('coopeforteApp.cliente.noCidade')}</label>
              <input id="noCidade"
                     className={errors?.endereco?.noCidade ? "is-invalid form-control" : "form-control"} {...register('endereco.noCidade')}
                     aria-invalid={errors?.endereco?.noCidade ? "true" : "false"}/>
              <div className="invalid-feedback">{errors?.endereco?.noCidade?.message}</div>
            </Col>
            <Col md={1}>
              <label htmlFor="coUf" className="form-label">{translate('coopeforteApp.cliente.coUf')}</label>
              <input id="coUf"
                     className={errors?.endereco?.coUf ? "is-invalid form-control" : "form-control"} {...register('endereco.coUf')}
                     aria-invalid={errors?.endereco?.coUf ? "true" : "false"}/>
              <div className="invalid-feedback">{errors?.endereco?.coUf?.message}</div>
            </Col>
            <Col md={5}>
              <label htmlFor="dsComplemento"
                     className="form-label">{translate('coopeforteApp.cliente.dsComplemento')}</label>
              <input id="dsComplemento"
                     className={errors?.endereco?.dsComplemento ? "is-invalid form-control" : "form-control"} {...register('endereco.dsComplemento')}
                     aria-invalid={errors?.endereco?.dsComplemento ? "true" : "false"}/>
              <div className="invalid-feedback">{errors?.endereco?.dsComplemento?.message}</div>
            </Col>
          </Row>

          &nbsp;
          <Row>
            <Col md={6}>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/cliente" replace color="info">
                <FontAwesomeIcon icon="arrow-left"/>
                &nbsp;
                <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.back">Back</Translate>
                    </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit"
                      disabled={updating}>
                <FontAwesomeIcon icon="save"/>
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </Col>
          </Row>

        </form>
    </div>
  );
};

export default ClienteUpdate;
