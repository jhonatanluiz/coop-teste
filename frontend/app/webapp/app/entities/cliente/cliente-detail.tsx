import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './cliente.reducer';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT, AUTHORITIES} from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import {cpfMask} from "app/shared/util/mask";
import {hasAnyAuthority} from "app/shared/auth/private-route";

export const ClienteDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const clienteEntity = useAppSelector(state => state.cliente.entity);
  const isAdmin = useAppSelector(state => hasAnyAuthority([state.authentication.account.perfil], [AUTHORITIES.ADMIN]));


  return (
    <Row>
      <Col md="8">
        <h2 data-cy="clienteDetailsHeading">
          <Translate contentKey="coopeforteApp.cliente.detail.title">Cliente</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{clienteEntity.id}</dd>
          <dt>
            <span id="noCliente">
              <Translate contentKey="coopeforteApp.cliente.noCliente">No Cliente</Translate>
            </span>
          </dt>
          <dd>{clienteEntity.noCliente}</dd>
          <dt>
            <span id="nuCpf">
              <Translate contentKey="coopeforteApp.cliente.nuCpf">Nu Cpf</Translate>
            </span>
          </dt>
          <dd>{cpfMask(clienteEntity.nuCpf + "" )}</dd>
        </dl>

        <Button tag={Link} to="/cliente" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;

        {isAdmin && <Button tag={Link} to={`/cliente/${clienteEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button> }
      </Col>
    </Row>
  );
};

export default ClienteDetail;
