export enum TipoTelefone {
  RESIDENCIAL = 'RESIDENCIAL',

  COMERCIAL = 'COMERCIAL',

  CELULAR = 'CELULAR',
}
