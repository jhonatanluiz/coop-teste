
export interface IEndereco {
  id?: number;
  nuCep?: string;
  dsLogradouro?: string;
  noBairro?: string;
  noCidade?: string;
  coUf?: string;
  dsComplemento?: string | null;
}

export const defaultValue: Readonly<IEndereco> = {};
