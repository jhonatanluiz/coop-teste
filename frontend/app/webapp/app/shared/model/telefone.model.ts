import {TipoTelefone} from "app/shared/model/enumerations/tipo-telefone.model";

export interface ITelefone {
  id?: number;
  nuTelefone?: string;
  coTipoTelefone?: TipoTelefone
}

export const defaultValue: Readonly<ITelefone> = {};
