import { IEndereco } from 'app/shared/model/endereco.model';
import { IEmail } from 'app/shared/model/email.model';
import {ITelefone} from "app/shared/model/telefone.model";

export interface ICliente {
  id?: number;
  noCliente?: string;
  nuCpf?: string;
  endereco?: IEndereco | null;
  emails?: IEmail[] | null;
  telefones?: ITelefone[] | null;
}

export const defaultValue: Readonly<ICliente> = {};
