
export interface IEmail {
  id?: number;
  dsEmail?: string;
}

export const defaultValue: Readonly<IEmail> = {};
