import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale from './locale';
import authentication from './authentication';
// prettier-ignore
import cliente from 'app/entities/cliente/cliente.reducer';
// prettier-ignore
import endereco from 'app/entities/endereco/endereco.reducer';


const rootReducer = {
  authentication,
  locale,
  cliente,
  endereco,
  loadingBar,
};

export default rootReducer;
