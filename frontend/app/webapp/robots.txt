# robotstxt.org/

User-agent: *
Disallow: /api/v1/account
Disallow: /api/account/change-password
Disallow: /api/account/sessions
Disallow: /api/logs/
Disallow: /api/users/
Disallow: /management/
Disallow: /v3/api-docs/
