## Tecnologias
### Backend
1. Java 11
2. SpringBoot V2.5.6
3. Junit (Testes Unitários)
4. Cucumber (Teste integração)
5. Flyway (Migrations)
6. Kafka (Mensageria)
7. Hazelcast (Cache)
8. Feign (Consumo de serviços)
9. Mysql/H2 (Banco de dados)
10. Swagger/OpenAPI(Documentação das API's)
11. Maven

### Frontend
1. React JS
2. Node JS
3. Jhispter
4. React Hook Form

### Execução Testes Unitários
1. Backend
```
mvn clean install
```
2. Frontend
```
npm run jest
```
### Scripts Banco de Dados
```

create table dbcoop.tb_cliente
(
    co_seq_cliente           int auto_increment
        primary key,
    no_cliente               varchar(100) not null,
    nu_cpf                   varchar(11)  not null,
    dt_criado                datetime(6)  not null,
    dt_modificado            datetime(6)  null,
    no_responsavel_cadastro  varchar(45)  not null,
    no_responsavel_alteracao varchar(45)  not null,
    constraint tb_cliente_nu_cpf_uindex
        unique (nu_cpf)
);

create table dbcoop.tb_email
(
    co_seq_email int auto_increment
        primary key,
    ds_email     varchar(150) not null,
    co_cliente   int          not null,
    constraint tb_email_ds_email_uindex
        unique (ds_email),
    constraint tb_email_tb_cliente_co_seq_cliente_fk
        foreign key (co_cliente) references dbcoop.tb_cliente (co_seq_cliente)
);

create table dbcoop.tb_endereco
(
    co_seq_endereco int auto_increment
        primary key,
    nu_cep          int          not null,
    ds_logradouro   varchar(100) not null,
    no_bairro       varchar(100) not null,
    no_cidade       varchar(100) not null,
    co_uf           varchar(2)   not null,
    ds_complemento  varchar(45)  null,
    co_cliente      int          not null,
    constraint tb_endereco_tb_cliente_co_seq_cliente_fk
        foreign key (co_cliente) references dbcoop.tb_cliente (co_seq_cliente)
);


create table dbcoop.tb_telefone
(
    co_seq_telefone  int auto_increment
        primary key,
    nu_telefone      varchar(15) not null,
    co_tipo_telefone tinyint(1)  not null,
    co_cliente       int         not null,
    constraint tb_telefone_tb_cliente_co_seq_cliente_fk
        foreign key (co_cliente) references dbcoop.tb_cliente (co_seq_cliente)
);

create table dbcoop.tb_usuario
(
    co_seq_usuario int auto_increment
        primary key,
    nm_usuario     varchar(100)      not null,
    ds_email       varchar(45)       not null,
    ds_senha       varchar(255)      not null,
    st_ativo       tinyint default 1 not null,
    co_perfil      int               not null,
    ds_login       varchar(45)       not null,
    constraint ds_email_UNIQUE
        unique (ds_email),
    constraint tb_usuario_ds_login_uindex
        unique (ds_login)
);

INSERT INTO dbcoop.tb_usuario (nm_usuario, ds_email, ds_senha, st_ativo, co_perfil, ds_login) VALUES ('admin', 'admin@coop.com', '$2a$10$Ke6svuB1KoNEwpe2iG7bteV98sNYxGTd6chhStGnG3h8deCpNssnq', 1, 0, 'admin');
INSERT INTO dbcoop.tb_usuario (nm_usuario, ds_email, ds_senha, st_ativo, co_perfil, ds_login) VALUES ('comum', 'comum@coop.com', '$2a$10$Ke6svuB1KoNEwpe2iG7bteV98sNYxGTd6chhStGnG3h8deCpNssnq', 1, 1, 'comum');
```
## Arquitetura do Projeto

### Cloud Native
1. Não mantém estado, não usa disco
2. Comunicação primária através de endpoints REST
3. Não tem dependência com sistema operacional
4. Desenvolvido para ser escalável horizontal e verticalmente

### Segurança
1. Sempre exige autenticação e autorização, segue os protocolos OAuth 2.0 e OpenIDConnect
2. Filtros de segurança OWASP
3. Permissões granulares

### Baixo acoplamento e Alta coesão
1. Ambiente de micro serviços
2. Sem lock-in com SGDBs
3. Aplicação do conceito de Bill of Materials
4. Aferição automatizada de métricas de qualidade
